" ========= Vundle ========= 
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
" Plugin 'tpope/vim-fugitive'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'

" NerdTree
Plugin 'scrooloose/nerdtree'
Plugin 'Xuyuanp/nerdtree-git-plugin'

" Auto save of buffer (pycharm like)
Plugin 'vim-scripts/vim-auto-save'


" Move in the file like vimperator
" Plugin 'easymotion/vim-easymotion'

Plugin 'kien/ctrlp.vim'
Plugin 'majutsushi/tagbar'
" Plugin 'Shougo/neocomplete.vim'
" Plugin 'marijnh/tern_for_vim'
" Plugin 'Valloric/YouCompleteMe'
" Plugin 'Chiel92/vim-autoformat'

" Plugin 'tpope/vim-dadbod'

Plugin 'airblade/vim-gitgutter'

" search in code with the ack search program
Plugin 'mileszs/ack.vim'

" Multi language async linter
" Plugin 'w0rp/ale'

" Python syntax hightlight improvement and autocompletion
" Plugin 'python-mode/python-mode'

" Python autocompletion
Plugin 'davidhalter/jedi-vim'


" # Extra Syntax
Plugin 'groenewege/vim-less'
Plugin 'mustache/vim-mustache-handlebars'

" Themes
Plugin 'rakr/vim-one'
Plugin 'endel/vim-github-colorscheme'
Plugin 'mswift42/vim-themes'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - inclosestalls plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

" ========== Configuration ============

syntax enable " Turn on syntax highlighting  
set hidden " Leave hidden buffers open  
set history=1000 "by default Vim saves your last 8 commands.  We can handle more
set number
set mouse=a
set nowrap
set tabstop=4
set shiftwidth=4
set expandtab
set hlsearch " highlight the current search string + '*' to search word under cursor to quickly find occurences of variable
set guioptions-=m  "remove menu bar
set guioptions-=T  "remove toolbar
set guioptions-=r  "remove right-hand scroll bar
set guioptions-=L  "remove left-hand scroll bar
set guifont=Noto\ Mono\ 9
colorscheme github
hi pythonSelf  ctermfg=68  guifg=#5f87d7 cterm=bold gui=bold


" ===== Mappings =====
" Navigate easily between split using arrow keys
" nnoremap <Down> <C-W><C-J>
" nnoremap <Up> <C-W><C-K>
" nnoremap <Right> <C-W><C-L>
" nnoremap <Left> <C-W><C-H>
let mapleader=","
nnoremap <C-j> 2<C-e><C-e>
nnoremap <C-k> 2<C-y><C-y>
nnoremap <C-h> :bp<CR>
nnoremap <C-l> :bn<CR>
nnoremap <space> z.
nnoremap <C-S> :mksession! /home/elie/sophia2/fwk.vim<CR>
nnoremap <C-Q> :qall<CR>
nnoremap <C-Tab> :bn<CR>
nnoremap <C-S-Tab> :bp<CR>
nnoremap <C-x> <C-o>
nnoremap <Tab><Tab> :CtrlPTag<CR>


" Copy / Paste with Ctrl C/V
vmap <C-c> "+y
vmap <C-x> "+c
vmap <C-v> c<ESC>"+p
imap <C-v> <ESC>"+pa

" ========= Jedi Vim ===========
let g:jedi#goto_command = "<C-a>"

" ========= AutoSave ===========
let g:auto_save = 1  " enable AutoSave on Vim startup
let g:auto_save_silent = 1  " do not display the auto-save notification

" ========= NERDTree ============

map <C-n> :NERDTreeToggle<CR>
"autocmd vimenter * NERDTree " automatically open NERDTree at startup
let g:NERDTreeIndicatorMapCustom = {
    \ "Modified"  : "✹",
    \ "Staged"    : "✚",
    \ "Untracked" : "✭",
    \ "Renamed"   : "➜",
    \ "Unmerged"  : "═",
    \ "Deleted"   : "✖",
    \ "Dirty"     : "✗",
    \ "Clean"     : "✔︎",
    \ "Unknown"   : "?"
    \ }

" ========= Vim Airline =========
" set laststatus=2 " always activate airline bar
let g:airline_powerline_fonts = 1 " activate powerline fonts (github.com/powerline/fonts) if installed
" let g:airline_theme='papercolor'
let g:airline#extensions#tabline#enabled = 1

set colorcolumn=120

" ========= CtrlP ==========
nnoremap <leader>p :CtrlP<CR>

" ======== TagBar =========
nnoremap <C-t> :TagbarToggle<CR>

" MySQL
" let g:dbext_default_profile = 'mysql'
" let g:dbext_default_profile_mysql = 'type=MYSQL:host=devtest-sophia-rw.priv.test.sewan.fr:port=3306:user=sophiamanager:password=CZG7YuTNCr:dbname=devtest_sophia'
